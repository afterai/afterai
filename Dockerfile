FROM archlinux/base

# Install useful development tools.
RUN pacman --noconfirm -S git go protobuf tree

WORKDIR /root
COPY . /root
