package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"

	"github.com/afterai/tensorshield/shields/predictshield"
	"google.golang.org/grpc"

	apis "tensorflow_serving/apis"
)

func main() {
	// Define command flags.
	flag.Usage = func() {
		fmt.Printf(`
%s starts a proxy server for a TensorFlow Serving instance that
intercepts, records, and validates incoming prediction requests.

`, os.Args[0])
		flag.PrintDefaults()
	}
	target := flag.String("target", "127.0.0.1:8500", "address of shielded TensorFlow Serving instance")
	listen := flag.String("addr", ":8000", "address of TensorShield server")
	flag.Parse()

	// Dial the target TensorFlow Serving PredictionService.
	conn, err := grpc.Dial(*target, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to target server: %v\n", err)
	}
	defer conn.Close()

	// Construct and register a TensorShield server.
	server := grpc.NewServer()
	apis.RegisterPredictionServiceServer(server, predictshield.New(conn))

	// Start the gRPC server.
	listener, err := net.Listen("tcp", *listen)
	if err != nil {
		log.Fatalf("Could not listen to TCP address: %v", err)
	}
	fmt.Printf("Listening at %s\n", *listen)
	server.Serve(listener)
}
