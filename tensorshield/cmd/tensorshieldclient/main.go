package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	flag.Usage = func() {
		fmt.Printf(`
%s is a test client for the TensorFlow Serving MNIST example
service. It intentionally creates malformed requests to test the TensorShield
tproxy.

`, os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
}
