# TensorShield

TensorShield is a proxy server for TensorFlow Serving services that intercepts,
records, and validates incoming prediction requests.

## Usage

### Running the MNIST prediction service

Run an example MNIST prediction service using TensorFlow Serving using
[their tutorial](https://www.tensorflow.org/tfx/serving/serving_basic):

1. Clone the repository:

   ```
   git clone https://github.com/tensorflow/serving.git
   cd serving
   rm -rf /tmp/mnist # Remove old MNIST training data if it exists
   ```

2. Train the example MNIST model:

   ```
   tools/run_in_docker.sh python tensorflow_serving/example/mnist_saved_model.py /tmp/mnist
   ```

3. Serve the model using TensorFlow ModelServer:

   ```
   docker run -p 8500:8500 --mount type=bind,source=/tmp/mnist,target=/models/mnist -e MODEL_NAME=mnist -t tensorflow/serving
   ```

### Running TensorShield

From within the TensorShield repository:

1. Build the TensorShield binary (this assumes you have Go builds working):

   ```
   go install ./...
   ```

2. Run TensorShield, providing the address of the ModelServer to shield:

   ```
   tensorshield -target=127.0.0.1:8500 -addr=:8000
   ```

### Testing TensorShield

From within `tensorflow/serving`:

Run the client, pointed at TensorShield:

```
tools/run_in_docker.sh python tensorflow_serving/example/mnist_client.py --num_tests=1000 --server=127.0.0.1:8000
```

TensorShield will log the requests that it proxies.

## Next steps

- Protobuf generation is an awful, terrible hack.
- `tensorshieldclient` needs to be implemented to generate test cases.
