package predictshield

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	apis "tensorflow_serving/apis"
)

var _ apis.PredictionServiceServer = &Server{}

// Server is a PredictionServiceServer that proxies to a target TensorFlow
// Serving PredictionService.
type Server struct {
	conn   *grpc.ClientConn
	client apis.PredictionServiceClient
}

// New initialises a Server given a dialed connection to a target TensorFlow
// Serving PredictionService.
func New(conn *grpc.ClientConn) *Server {
	return &Server{
		conn:   conn,
		client: apis.NewPredictionServiceClient(conn),
	}
}

func (s *Server) Predict(ctx context.Context, req *apis.PredictRequest) (*apis.PredictResponse, error) {
	log.Println("Request received")

	inputs := req.GetInputs()
	dims := inputs["images"].TensorShape.GetDim()
	x := dims[0]
	y := dims[1]

	if x.Size != 1 {
		log.Println("Warning: first dimension was not of size 1")
	}
	if y.Size != 784 {
		log.Println("Warning: second dimension was not of size 784")
	}

	return s.client.Predict(ctx, req)
}

// Classify is not implemented.
func (s *Server) Classify(context.Context, *apis.ClassificationRequest) (*apis.ClassificationResponse, error) {
	return nil, grpc.Errorf(codes.Unimplemented, "not implemented")
}

// Regress is not implemented.
func (s *Server) Regress(context.Context, *apis.RegressionRequest) (*apis.RegressionResponse, error) {
	return nil, grpc.Errorf(codes.Unimplemented, "not implemented")
}

// MultiInference is not implemented.
func (s *Server) MultiInference(context.Context, *apis.MultiInferenceRequest) (*apis.MultiInferenceResponse, error) {
	return nil, grpc.Errorf(codes.Unimplemented, "not implemented")
}

// GetModelMetadata is not implemented.
func (s *Server) GetModelMetadata(context.Context, *apis.GetModelMetadataRequest) (*apis.GetModelMetadataResponse, error) {
	return nil, grpc.Errorf(codes.Unimplemented, "not implemented")
}
