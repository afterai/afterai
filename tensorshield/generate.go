package tensorshield

//go:generate mkdir -p vendor
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/serving/tensorflow_serving/apis/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/serving/tensorflow_serving/config/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/serving/tensorflow_serving/core/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/serving/tensorflow_serving/util/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/serving/tensorflow_serving/sources/storage_path/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/tensorflow/tensorflow/core/framework/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/tensorflow/tensorflow/core/example/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/tensorflow/tensorflow/core/lib/core/*.proto"
//go:generate bash -c "protoc -I ../third_party/github.com/tensorflow/tensorflow -I ../third_party/github.com/tensorflow/serving --go_out=plugins=grpc:vendor ../third_party/github.com/tensorflow/tensorflow/tensorflow/core/protobuf/*.proto"
